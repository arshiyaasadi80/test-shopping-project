//
// Global Types

export interface ToastMessageProps {
    type: string | null
    message: string | null
}

export interface LayoutDashboardProps {
    children: any
    onRefresh?: any
    error?: any
    loading?: boolean
    setNotify?: ToastMessageProps | null
}

//
// API Types
export interface Category {
    _id?: string
    name?: string
    slug?: string
}

export interface Creator {
    role?: string
    _id?: string
    name?: string
}

export interface Product {
    _id?: string
    title?: string
    price?: string | number
    category?: Category
    description?: string
    createdBy?: Creator
    createdAt?: string
    updatedAt?: string
    slug?: string
    image?: string
}