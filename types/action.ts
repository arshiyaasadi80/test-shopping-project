//
// action types
import { Product } from './global'
import {
    FETCH_PRODUCT_REQUEST,
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCT_FAILURE,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    INCREASE_CART_ITEM,
    DECREASE_CART_ITEM,
    FETCH_CART_DATA,
} from "../store/actionTypes"

export interface Cart {
    _id?: any | string
    number?: number
    item?: Product
    createdAt?: any | string
}

//
// Product State
//
export interface ProductState {
    pending: boolean
    products: Product[]
    error: string | null
}

export interface FetchProductSuccessPayload {
    products: Product[]
}
export type FetchProductSuccess = {
    type: typeof FETCH_PRODUCT_SUCCESS
    payload: FetchProductSuccessPayload
}

export interface FetchProductFailurePayload {
    error: string
}
export type FetchProductFailure = {
    type: typeof FETCH_PRODUCT_FAILURE
    payload: FetchProductFailurePayload
}

export interface FetchProductRequest {
    type: typeof FETCH_PRODUCT_REQUEST
}

export type ProductActions = | FetchProductRequest | FetchProductSuccess | FetchProductFailure

//
// Cart State
//
export interface CartState {
    cart: Cart[]
}

export interface FetchCartDataPayload {
    cart: Cart[]
}
export type FetchCartData = {
    type: typeof FETCH_CART_DATA
    payload: FetchCartDataPayload
}

export interface FetchCartItemPayload {
    product: Product | undefined
}
export type FetchCartItemData = {
    type: typeof ADD_TO_CART | typeof REMOVE_FROM_CART | typeof INCREASE_CART_ITEM | typeof DECREASE_CART_ITEM
    payload: FetchCartItemPayload
}

export type CartActions = | FetchCartData | FetchCartItemData