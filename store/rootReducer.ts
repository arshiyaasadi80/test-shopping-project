import { combineReducers } from "redux"

import productReducer from "./app/reducer"
import cartReducer from "./cart/reducer"

const rootReducer = combineReducers({
    appData: productReducer,
    cartData: cartReducer,
});

export type AppState = ReturnType<typeof rootReducer>

export default rootReducer