import {
    FETCH_CART_DATA,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    INCREASE_CART_ITEM,
    DECREASE_CART_ITEM,
} from "../actionTypes"

import {Cart, CartState, CartActions} from "../../types/action"

// store items
const initialState: CartState = {
    cart: []
};

export default (state = initialState, action: CartActions) => {
    let newCart: Cart[] | any = [...state.cart]
    switch (action.type) {
        case FETCH_CART_DATA:
            // Fetch cart data
            const { cart } = action.payload
            return {
                ...state,
                cart,
            }

        case ADD_TO_CART:
            // Add item to cart
            let newCartItem: Cart = {
                _id: action.payload.product?._id,
                number: 1,
                item: action.payload.product,
                createdAt: new Date()
            }
            return {
                ...state,
                cart: [...state.cart, newCartItem]
            }

        case INCREASE_CART_ITEM:
            // Increase item to cart

            // Check exist item in cart
            const isExistA: any = state.cart.find((item: Cart) => item._id === action.payload.product?._id)
            if (isExistA) {
                newCart = state.cart.map((item: Cart) => {
                    if(item._id === isExistA._id) {
                        // @ts-ignore
                        item.number ++
                        return item
                    } else{
                        return item
                    }
                })
            }
            return {
                ...state,
                cart: newCart
            }

        case REMOVE_FROM_CART:
            // Remove item to cart
            const isExistB: any = state.cart.find((item: Cart) => item._id === action.payload.product?._id)
            newCart.splice(isExistB, 1)
            return {
                ...state,
                cart: newCart
            }

        case DECREASE_CART_ITEM:
            // Decrease item to cart
            const isExistC: any = state.cart.find((item: Cart) => item._id === action.payload.product?._id)
            // if (isExist && isExist?.number > 1) {
                newCart = state.cart.map((item: Cart) => {
                    if(item._id === isExistC._id) {
                        // @ts-ignore
                        item.number --
                        return item
                    } else{
                        return item
                    }
                })
            // }
            return {
                ...state,
                cart: newCart
            }

        default:
            return {
                ...state,
        }
    }
}