import {
    FETCH_CART_DATA,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    INCREASE_CART_ITEM,
    DECREASE_CART_ITEM,
} from "../actionTypes"

import {
    FetchCartData,
    FetchCartDataPayload,
    FetchCartItemData,
    FetchCartItemPayload,
} from "../../types/action"


export const fetchCartData = (
    payload: FetchCartDataPayload
): FetchCartData => ({
    type: FETCH_CART_DATA,
    payload,
})

export const addCartItem = (
    payload: FetchCartItemPayload
): FetchCartItemData => ({
    type: ADD_TO_CART,
    payload,
})
export const increaseCartItem = (
    payload: FetchCartItemPayload
): FetchCartItemData => ({
    type: INCREASE_CART_ITEM,
    payload,
})
export const removeCartItem = (
    payload: FetchCartItemPayload
): FetchCartItemData => ({
    type: REMOVE_FROM_CART,
    payload,
})
export const decreaseCartItem = (
    payload: FetchCartItemPayload
): FetchCartItemData => ({
    type: DECREASE_CART_ITEM,
    payload,
})
