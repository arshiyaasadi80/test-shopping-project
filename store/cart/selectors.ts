import { createSelector } from "reselect"

import { AppState } from "../rootReducer"

const getCart = (state: AppState) => state.cartData.cart
export const getCartSelector = createSelector(getCart, (cart) => cart)