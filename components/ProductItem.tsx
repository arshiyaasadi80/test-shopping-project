//
// Products card
import React from "react"
import styles from "../styles/components/Product.module.sass"
import Image from "next/image"
import images from "../public/images"
import Link from "next/link"
import { useRouter } from "next/router"
import { Product } from "../types/global"
import { Cart } from "../types/action"

// redux
import { useDispatch, useSelector } from "react-redux"
import { addCartItem, removeCartItem, increaseCartItem, decreaseCartItem } from "../store/cart/actions"
import { getCartSelector } from "../store/cart/selectors"

const ProductItem = ({ productData }: { productData: Product }) => {
    const router = useRouter()
    const dispatch = useDispatch()
    const cart: Cart[] = useSelector(getCartSelector)
    const {_id, title, price, category, description, createdBy, image} = productData

    // Check exist item in cart
    const isProductInCart = () => {
        return cart.find((item: Cart) => item._id === _id)
    }

    // Add Item to cart in redux
    const handleAddCartItem = (product: Product= productData) =>{
        dispatch(addCartItem({product}))
    }

    // Increase item to cart in redux
    const handleIncreaseCartItem = (product: Product= productData) =>{
        dispatch(increaseCartItem({product}))
    }

    // Remove Item from cart in redux
    const handleRemoveCartItem = (product: Product= productData) =>{
        dispatch(removeCartItem({product}))
    }

    // Decrease Item from cart in redux
    const handleDecreaseCartItem = (product: Product= productData) =>{
        dispatch(decreaseCartItem({product}))
    }

    return (
        <div key={_id} className={styles.cardContainer}>
            {/* Header */}
            <div className={styles.cardImage}>
                {/* Image */}
                <span className={styles.cardCover}>
                    {
                        image ? (
                            <img src={image} alt={"product"} />
                        ) : (
                            // Show default app image
                            <Image src={images.product} alt={`item: ${title}`} />
                        )
                    }
                </span>

                {/* Information */}
                <Link href={`/product/${_id}`}>
                    <a>
                        <div className={styles.productInfo}>
                            <div>
                                <span>
                                    {
                                        image ? (
                                            <Image src={image} alt={`item: ${title}`} />
                                        ) : (
                                            // Show default app image
                                            <Image src={images.product} alt={`item: ${title}`} />
                                        )
                                    }
                                </span>
                                <div>
                                    <h4>{title}</h4>
                                    <p>{createdBy?.name}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </Link>
            </div>
            {/* Description */}
            <div>
                <p>{title}</p>
                <div>
                    <span>
                        {description}
                        <div>
                                category:
                                <span>
                                    {category?.name}
                                </span>
                            </div>
                    </span>
                    <div className={styles.rate}>
                        <div>
                            <div className={styles.buyBtn}>
                                    {
                                        isProductInCart() ? (
                                            <>
                                                {
                                                    // @ts-ignore
                                                    isProductInCart()?.number > 1 ? (
                                                        <span onClick={()=> handleDecreaseCartItem(productData)}>-</span>
                                                    ) : (
                                                        <span onClick={()=> handleRemoveCartItem(productData)}>remove</span>
                                                    )
                                                }
                                                <span>{isProductInCart()?.number}</span>
                                                <span onClick={()=> handleIncreaseCartItem(productData)}>+</span>
                                            </>
                                        ) : (
                                            <span onClick={()=> handleAddCartItem(productData)}>
                                                Buy
                                            </span>
                                        )
                                    }
                            </div>
                        </div>
                        <div>
                            <span>
                                {price}
                            </span>$
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductItem