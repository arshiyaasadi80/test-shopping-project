import '../styles/globals.sass'
import type { AppProps } from 'next/app'
import { wrapper } from "../store"
// import { Provider } from "react-redux"
import { ToastContainer } from "react-toastify"
import { Slide } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"

function MyApp({ Component, pageProps }: AppProps) {
  return (
      <>
        <Component {...pageProps} />

          {/* toasts */}
          <ToastContainer
              position="bottom-left"
              autoClose={3000}
              transition={Slide}
              hideProgressBar={false}
              newestOnTop={false}
              draggableDirection="y"
              closeOnClick
          />
      </>
  )
}

// export default MyApp
export default wrapper.withRedux(MyApp)
