//
// Cart page
import React, {useState} from "react"
import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../../styles/pages/Cart.module.sass'
import Layout from "../../components/Layout"
import Image from "next/image"
import images from "../../public/images"
import {Product, ToastMessageProps} from "../../types/global"
import { Cart } from "../../types/action"

// redux
import { useDispatch, useSelector } from "react-redux"
import { getCartSelector } from "../../store/cart/selectors"
import { decreaseCartItem, fetchCartData, increaseCartItem, removeCartItem } from "../../store/cart/actions"

const Cart: NextPage = () => {
    const dispatch = useDispatch()
    const cart: Cart[] = useSelector(getCartSelector)
    const [notify, setNotify] = useState<ToastMessageProps | null>(null)

    // Increase item to cart in redux
    const handleIncreaseCartItem = (product: Product | undefined) =>{
        dispatch(increaseCartItem({product}))
    }

    // Remove Item from cart in redux
    const handleRemoveCartItem = (product: Product | undefined) =>{
        dispatch(removeCartItem({product}))
    }

    // Decrease Item from cart in redux
    const handleDecreaseCartItem = (product: Product | undefined) =>{
        dispatch(decreaseCartItem({product}))
    }


    // Get total price from cart item
    const getTotal = () => {
        return cart.reduce((total: any, item: any) => total + (+item.number * +item.item.price), 0)
    }

    // continue
    const buyCart = () => {
        setNotify({type: 'success', message: 'success !'})
        localStorage.removeItem('shoppingCart')
        dispatch(fetchCartData({cart: []}))
    }

    return (
        <Layout setNotify={notify}>
            <Head>
                <title>Shopping website | Cart</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div className={styles.main}>
                <h1 className={styles.title}>
                    Cart
                </h1>

                <div className={styles.cartContainer}>

                    {/* Cart order section */}
                    <div className={styles.cartOrders}>
                        {
                            cart.map((item: Cart, index: number)=> {
                                return(
                                    <div key={index}>
                                        <span>
                                            <Image src={images?.product} alt={``} layout={"fill"} />
                                        </span>
                                        <div>
                                            <div>
                                                <span>title: {item?.item?.title}</span>
                                                <span>price: {item?.item?.price}$</span>
                                            </div>
                                            <div>
                                                {
                                                    // @ts-ignore
                                                    item?.number > 1 ? (
                                                        <span onClick={()=> handleDecreaseCartItem(item?.item)}>-</span>
                                                    ) : (
                                                        <span onClick={()=> handleRemoveCartItem(item?.item)}>remove</span>
                                                    )
                                                }
                                                <span>{item?.number}</span>
                                                <span onClick={()=> handleIncreaseCartItem(item?.item)}>+</span>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>

                    {/* Cart info section */}
                    <div className={styles.cartInfo}>
                        <div>
                            <span>total cart</span>
                            <span>{getTotal()}$</span>
                        </div>
                        <div>
                            <span>discount</span>
                            <span>0$</span>
                        </div>
                        <div>
                            <span>subtotal</span>
                            <span>0$</span>
                        </div>
                        <div className={styles.checkOut}>
                            <button disabled={!cart?.length} onClick={()=> buyCart()}>continue</button>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Cart
